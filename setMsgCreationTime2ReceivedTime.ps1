<###################################################################################################

    powershell script to update creation and write time of locally saved outlook msg files

----------------------------------------------------------------------------------------------------

    Copyright (c) 2017 Stephan Rau published under the MIT License

----------------------------------------------------------------------------------------------------

    runs recursively through the directory where the script was called and uses ms outlook
    to read the received time of the msg file

    if outlook is open before the script is started it will close outlook when finished
        somehow outlook object does not seperate the new outlook object from the existing one
        who ever nows how to improve: feel free to do and commit to share

    examples on how to run
        copy script to a folder with msg files and do right click -> "Run with PowerShell" 
        start from power shell
           PS ...> & ..\scripts\setMsgCreationTime2ReceivedTime.ps1

###################################################################################################>

# open an outlook instance to read .msg properties
$outlook = New-Object -comobject outlook.application

# get all msg files in current directory
$SourceFiles = Get-ChildItem -Recurse . -Filter *.msg

# run through all msg files and set creation date
foreach ( $file in $SourceFiles ) {
	# open msg with outlook
	$msg = $outlook.CreateItemFromTemplate($file.fullname)

	# print the current msg file name and the received time
	Write-Host $msg.ReceivedTime ": $file"

	# set the msg file creation time to received time
	Set-ItemProperty $file.fullname -name CreationTime -value $msg.ReceivedTime
	Set-ItemProperty $file.fullname -name LastWriteTime -value $msg.ReceivedTime
}

# close outlook instance
$outlook.Quit()
